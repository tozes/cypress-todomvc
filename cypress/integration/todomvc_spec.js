/// <reference types="Cypress" />

context('Todo MVC testcases', () => {

  beforeEach(() => {
    cy.visit('/')
  })

  context('Page content', () => {

    it('Should contain visible title', () => {
      cy.get('h1[data-cy="title"]')
      .should('be.visible')
      .contains('todos')
    })

    it('Should contain the input field', () => {
      cy.get('input[data-cy="new-todo-input"]')
      .should('be.visible')
    })

    it('Task list and footer should not be visible', () => {
      cy.get('.footer').should('not.exist')
      cy.get('.view').should('not.exist')
    })

  })

  context('Task input field', () => {

    it('Contains Placeholder text', () => {
      cy.get('.new-todo')
        .should('has.attr', 'placeholder', 'What needs to be done?')
    })

    it('Should be possible to input and delete text', () => {
      cy.get('.new-todo')
      .type('Task_1')
      .should('has.attr', 'value', 'Task_1')
      .type('{backspace}{backspace}')
      .should('has.attr', 'value', 'Task')
    })

    it('Clears when the user submits a task', () => {
      cy.get('.new-todo')
        .type('Task 1{enter}')
        .should('has.attr', 'value', '')
    })

  })

  context('Task list', () => {

    it('Should display main, footer and task when adding first task', () => {
      cy.addTask('Task 1')
      cy.get('.view').should('exist').and('be.visible')
      cy.get('.footer').should('exist').and('be.visible')
      cy.get('li[data-cy="todo-item"]')
        .should('have.length', 1)
        .find('label')
          .should('contain', 'Task 1')
    })

    it('New tasks are added to the end of the list', () => {
      cy.wrap([0, 1, 2]).each(task_number => {
        cy.addTask('Task ' + task_number)
        cy.get('li[data-cy="todo-item"]').eq(task_number)
        .find('label')
          .should('contain', 'Task ' + task_number)
      })
    })

    it('Displays the correct number of tasks after adding/deleting tasks', () => {
      cy.wrap([1, 2, 3]).each(task_number => {
        cy.addTask('Task ' + task_number)
        cy.get('li[data-cy="todo-item"]')
          .should('have.length', task_number)
      })
      cy.get('li[data-cy="todo-item"]').eq(0)
        .find('.destroy')
          .invoke('show')
          .click()
      cy.get('li[data-cy="todo-item"]')
        .should('have.length', 2)
    })

    it('Deleting all tasks hides the task list and the footer', () => {
      cy.addTask('Task 1')
      cy.get('li[data-cy="todo-item"]').eq(0)
        .find('.destroy')
          .invoke('show')
          .click()
      cy.get('.footer').should('not.exist')
      cy.get('.view').should('not.exist')
    })

  })

  context('Task operations', () => {

    it('New tasks are not marked as completed by default', () => {
      cy.addTask('Task 1')
      cy.get('li[data-cy="todo-item"]')
        .should('not.have.attr', 'class', 'completed')
        .find('.toggle')
        .should('not.be.checked')
    })


  })
})
