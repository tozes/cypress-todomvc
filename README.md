## Test plan

#### Page content
  - Contains visible title
  - Contains Task input field
  - Task list and footer not visible when there are no tasks
  - (with persistent storage) Task list with expected tasks and footer are displayed on page load

#### Task input field
  - Contains placeholder text when there's no input (both on page load and after being cleared either by deleting input manually or after addind a task)
  - It's possible to type into input field
  - It's possible to delete text from the input field
  - The field clears up when the user presses Enter to add the task

#### Task list
  - When adding first task, check that everything is displayed (task list, footer, footer contents)
  - Number of tasks displayed is consistent with number of tasks added
  - New tasks are added to the bottom of the list
  - Page can be scrolled when there are many tasks
  - Delete tasks
    - Number of displayed tasks is consistent with expected
    - Deleting all tasks hides the task list and footer

#### Task operations
  - Adding a task
    - Displays the correct task name
    - The task is not marked as completed by default
      - No strikethrough style
      - Checkmark not visible
  - Mark task as completed
    - Strikethrough style
    - Checkmark visible
  - Rename task
    - Double click opens edit mode
    - New task name is applied after pressin Enter
  - Delete task
    - Delete button not visible by default
    - Delete button shows on task hover
    - Task is deleted when the Delete button is pressed
  - Task status toggle button
    - Visible only if the list contains tasks
    - If there's at least one incomplete task, mark everything as completed
    - If everything is completed, mark everything as incomplete

#### Task counter
  - Displays the expected number of tasks
    - Increases as tasks are added
    - Decreases as tasks are deleted
    - Decreases as tasks are maked as done
  - Displays 'No tasks left' when all tasks are completed


#### Filters
  - Active filter
    - All incomplete tasks should be visible
    - No completed tasks should be visible
  - Completed filter
    - All completed tasks should be visible
    - No incomplete tasks should be visible
  - All filter
    - Returning to All after using any of the other filters should show everything
  - The footer is displayed even if the current filter hides all tasks

#### Footer
  - Option to clear completed tasks
    - Shows when there's at least one completed task
    - Disappears when there are no completed tasks
      - By clicking on it
      - By unmarking all completed tasks back to incomplete
    - Pressing it deletes all completed tasks




## Product board multi-select testcases

- Single note selection (without multiselect mode activated)
  - On page load, the top note should be selected
  - Single selection works by pressing anywhere inside the note component
  - The highlighted note has blue background and blue border
    - The background of the selected note gets white when hovered
    - The blue border remains when hovered 
  - The right side panel shows in depth details about the selected note
    - All the details are shown - title, source, tags, description, comments, etc.
  - Clicking on another note changes the selection
    - The clicked note is highlighted in blue
    - The previously selected note is no longer highlighted in blue
    - The right side panel now shows information about the newly selected note

- Multiple selection
  - Different ways to initiate multiple selection
    - Hovering the icon
      - When hovering over it, the icon should disappear and show the select checkbox
    - By Pressing shift or cmd/ctrl
      - While pessing shift/cmd/ctrl, the select checkbox should appear when hovering anywhere in the note component
      - Clicking anywhere in the component should select the current note and activate the selection mode

  - Behaviour while holding cmd/ctrl
    - Any random note which is not selected can be added to the selection by clicking in its card component
    - Any random note which is selected can be removed from the selection by clicking in its card component

  - Behaviour while holding shift
    - When clicking a note, any selection for a sequence of notes between the last one that was interacted with and the one we're currently clicking should be toggled
      - If the note we're clicking was selected, all will be unselected
      - If the note we're clicking was unselected, all will be selected

  - Toggle selection while multi selection is active
    - If only one note is selected and it's unselected, multiple selection mode is deactivated
    - Note selection can be toggled by pressing anywhere inside a note component

  - If only one note is selected when multiple selection mode is active, the right side panel doesn't display that note's details

  - When multiple selection is active, the right side panel shows:
    - the # of selected notes
    - the person that sent the note if only one is selected
    - 'Said by multiple' if multiple notes are selected
    - Shows any common tags that all selected notes share
  
  - Bonus
    - When not in multi selection mode, while pressing shift and selecting a card other than the already selected one, should trigger multi-selection mode and select all the notes in between

